<?php
    require_once('db_functions.php');
    header('Access-Control-Allow-Origin: *');
    $db_funs = new DB_Functions();
    header('Content-Type: application/json');
    $module = '';
    $operation = '';
    if (isset ($_GET['module'])) {
    	$module = trim($_GET['module']);
    } else if (isset ($_POST['module'])) {
    	$module = trim($_POST['module']);
    }
    
    if (isset ($_GET['operation'])) {
    	$operation = trim($_GET['operation']);
    } else if (isset ($_POST['operation'])) {
    	$operation = trim($_POST['operation']);
    }
    
    switch($operation){
        case 'registerTba':
            //Get details to register
            echo "Right operation";
            if(isset($_GET['Id'])){
                $Id = $_GET['Id'] ;
                $FName = $_GET['FirstName'] ;
                $LName = $_GET['LastName'] ;
                $Phone = $_GET['PhoneNumber'] ;
                $Location = $_GET['Location'] ;
                $Registered = $_GET['RegisteredBy'];
                $Years = $_GET['YearsInPractise'] ;
                $response  = $db_funs->registerTBA($Id,$FName,$LName,$Phone,$Location,$Years,$Registered,"Low");
                echo json_encode($response);
            }else{
                $Id = $_POST['Id'] ;
                $FName = $_POST['FirstName'] ;
                $LName = $_POST['LastName'] ;
                $Phone = $_POST['PhoneNumber'] ;
                $Location = $_POST['Location'] ;
                $Registered = $_POST['RegisteredBy'];
                $Years = $_POST['YearsInPractise'] ;
                $response  = $db_funs->registerTBA($Id,$FName,$LName,$Phone,$Location,$Years,$Registered,"Low");
                echo json_encode($response);
            }

            break;
            
        case 'getTbas':
            $response = $db_funs->getAllTbas();
            echo json_encode($response);
            break;
            
        case 'getTbaByNumber':
            if(isset($_GET['PhoneNumber'])){
                $Phone = $_GET['PhoneNumber'];
                $response = $db_funs->getTbaByPhoneNumber($Phone);
                echo json_encode($response);
            }else{
                $Phone = $_POST['PhoneNumber'];
                $response = $db_funs->getTbaByPhoneNumber($Phone);
                echo json_encode($response);
            }
            break;
            
        case 'getPregnancyComments':
            if(isset($_GET['Phone'])){
                $Id = $_GET['MotherId'];
                $response = $db_funs->getAllCommentsByMother($Id);
                echo json_encode($response);
            }else{
                $Id = $_POST['MotherId'];
                $response = $db_funs->getAllCommentsByMother($Id);
                echo json_encode($response);
            }
            break;
        case 'getMothers':
            $response = $db_funs->getAllMothers();
            echo json_encode($response);
            break;
                
    }
    
?>