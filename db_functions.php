<?php
    
    class DB_Functions{
        private $con;
        function __construct(){
            require_once ('Connect.php');
            require_once('AfricasTalkingGateway.php');
            require_once('config.php');
            $dbcon = new DB_Connect();
            $this->con = $dbcon->connect();
        }
        
        public function registerTBA($Id,$FName,$LName,$Phone,$Location,$Years,$RegisterId,$Training){
            $stmt = $this->con->prepare("INSERT INTO Tba(Id,FirstName,LastName,PhoneNumber,Location,YearsInPractise,RegisteredBy,Training) VALUES(?,?,?,?,?,?,?,?) ");
            $stmt->bind_param("ssssssss",$Id,$FName,$LName,$Phone,$Location,$Years,$RegisterId,$Training);
            $result = $stmt->execute();
            $stmt->close();
            
            if ($result) {
              return true;
            } else {
              return false;
            }
        }
        
        //Register Mom
        public function registerMotherTba($Name,$Location,$Comment){
            $stmt = $this->con->prepare("INSERT INTO Mothers(Name,Location,PregnancyComment) VALUES(?,?,?) ");
            $stmt->bind_param("sss",$Name,$Location,$Comment);
            $result = $stmt->execute();
            $stmt->close();
            
            if ($result) {
              return true;
            } else {
              return false;
            }
        }
        
        //Add Comment
        public function addComment($Author,$AuthorId,$Comment){
          $stmt = $this->con->prepare("INSERT INTO PregnancyComment(Author,AuthorId,Comment) VALUES(?,?,?)");
          $stmt->bind_param("sss",$AuthorId, $Author,$Comment);
          $result = $stmt->execute();
          $stmt->close();
          
          if ($result) {
              return true;
            } else {
              return false;
            }
        }
        
        
        //Get All Tbas
        public function getAllTbas(){
          $tbas = array();
          $i = 0;
          $stmt = $this->con->prepare("SELECT * FROM Tba");
          $exec = $stmt->execute();
          $stmt->bind_result($Id,$FName,$LName,$Phone,$Location,$Years,$Training,$Registered,$Date);
          
          if($exec){
            while ($result = $stmt->fetch()) {
            $tbas[$i]["Id"]= $Id;
            $tbas[$i]["FirstName"] = $FName;
            $tbas[$i]["LastName"] = $LName;
            $tbas[$i]["PhoneNumber"] = $Phone;
            $tbas[$i]["Location"] = $Location;
            $tbas[$i]["YearsInPractise"] = $Years;
            $tbas[$i]["Training"] = $Training;
            $tbas[$i]['MothersLinkedTo'] = 0;
            $tbas[$i]["Date"] = $Date;
            $i++;
            }
            $stmt->close();
              for($i = 0; $i < sizeof($tbas); $i++){
                $stmt2 = $this->con->prepare("SELECT Name FROM Mothers WHERE TbaId=?");
                $Id2 = $tbas[$i]["Id"];
                $stmt2->bind_param("s",$Id2);
                $stmt2->execute();
                $stmt2->store_result();
                $Connected = $stmt2->num_rows;
                $tbas[$i]['MothersLinkedTo'] = $Connected;
                $stmt2->close();
            }
             
            $Id2 = 0;
            
          return $tbas;
  
          }else {
            return false;
          }
          
        }
        
        //Get Tba by phone Number
        public function getTbaByPhoneNumber($Phone){
          $stmt = $this->con->prepare("SELECT * FROM Tba WHERE PhoneNumber=? ");
          $stmt->bind_param("s",$Phone);
          if($stmt->execute()){
            $result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
    
            return $result;
          }else {
          return false;
          }
        }
        
        //Get all pregnancy comments
        public function getAllCommentsByMother($MotherId){
          $stmt = $this->con->prepare("SELECT * FROM PregnancyComment WHERE MotherId=? ");
          $stmt->bind_param("s",$MotherId);
          if($stmt->execute()){
            $result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
    
            return $result;
          }else {
          return false;
          }
        }
        //Get all Mothers
        public function getAllMothers(){
          $mothers = array();
          $i = 0;
          $stmt = $this->con->prepare("SELECT * FROM Mothers");
          $exec = $stmt->execute();
          $stmt->bind_result($Id,$Name,$Location,$Date,$TbaId,$Session);
          if($exec){
            while ($result = $stmt->fetch()) {
              $mothers[$i]["Id"] = $Id;
              $mothers[$i]["Name"] = $Name;
              $mothers[$i]["Location"] = $Location;
              $mothers[$i]["Date"] = $Date;
              $mothers[$i]["TbaName"] = "TBA";
              $mothers[$i]["ConnectedTo"] = $TbaId;
              $i++;
            }
            $stmt->close();
            
            for($i = 0;$i<sizeof($mothers); $i++){
              $stmt = $this->con->prepare("SELECT * FROM Tba WHERE Id=?");
              $stmt->bind_param("s",$mothers[$i]["ConnectedTo"]);
              //$stmt->bind_result();
              $stmt->execute();
              $name = $stmt->get_result()->fetch_assoc();
              //echo($name["FirstName"]);
              $mothers[$i]["TbaName"] = $name["FirstName"]. " " . $name["LastName"];
            }
            
            return $mothers;
            
          }else{
            return false;
          }
        }
        //Get all those who have subscribed for education
        public function getAllSubscriptions(){
          $stmt = $this->con->prepare("SELECT * FROM Education_Subscription ");
          if($stmt->execute()){
            $result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
    
            return $result;
          }else {
          return NULL;
          }
        }
        
        
        
        //Get Subscription from phone number
        public function getSubscriptionByPhone($Phone){
          $stmt = $this->con->prepare("SELECT * FROM Education_Subscription WHERE PhoneNumber=? ");
          $stmt->bind_param("s",$Phone);
          if($stmt->execute()){
            $result = $stmt->get_result()->fetch_assoc();
            $stmt->close();
    
            return $result;
          }else {
          return NULL;
          }
        }
        
        //Unsubscribe from Education service
        public function unsubscribe($Phone){
          $status = 0;
          $stmt = $this->con->prepare("UPDATE  Education_Subscription SET Status=? WHERE PhoneNumber=?");
          $stmt->bind_param("ss",$status,$Phone);
          $result = $stmt->execute();
          $stmt->close();
          
          if($result){
            return true;
          }else {
          return false;
          }
        }
        
    }
?>