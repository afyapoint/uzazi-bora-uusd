    <?php
    if(!empty($_POST)){
        require_once('db_connect.php');
        require_once('AfricasTalkingGateway.php');
        require_once('config.php');
        
        //receiving the POST from AT
        $sessionId=$_POST['sessionId'];
        $serviceCode=$_POST['serviceCode'];
        $phoneNumber=$_POST['phoneNumber'];
        $text=$_POST['text'];
        
        //Explode to get the value of the latest interaction think 1*1
        $textArray = explode('*', $text);
        
        $userResponse = trim(end($textArray));
        
        //Check the level
        $level = 0;
        $sql = "select `level` from `session_levels` where `session_id`='" . $sessionId . "'";
        $levelQuery = $db->query($sql);
        if($result = $levelQuery->fetch_assoc()) {
          $level = $result['level'];
        }
        
        //check if user is not in db
        $firstQuery="SELECT * FROM Tba WHERE `PhoneNumber` LIKE '%".$phoneNumber."%' LIMIT 1";
        $firstResult=$db->query($firstQuery);
        $userAvail=$firstResult->fetch_assoc();
        
        if($userAvail && $userAvail['Name']!=NULL) {
            if($userResponse=="" || $userResponse=="0" && $level == 0){
                //Graduate the user to the next level, so you dont serve them the same menu
                 $s1Query = "insert into `session_levels`(`session_id`, `phoneNumber`,`level`)
                    values('".$sessionId."','".$phoneNumber."', 1)";
                 $db->query($s1Query);
                  //Serve our services menu
                  $response = "CON Karibu " . $userAvail['username']  . ". Please choose a service.\n";
                  $response .= " 1. Send me todays voice tip.\n";
                  $response .= " 2. Please call me!\n";
                  $response .= " 3. Send me Airtime!\n";
            }elseif ($level == 1) {
                    if($userResponse ==NULL){
                        $response = "CON You have to choose a service.\n";
                        $response .= " Press 0 to go back.\n";
                    
                        //Demote the user to level 0
                        $s1levelUpdate = "update `session_levels` set `level`=0 where `session_id`='".$sessionId."'";
                        $db->query($s1levelUpdate);
                    
                        }elseif($userResponse == "1"){
                        $response = "END Please check your SMS inbox.\n";
                    
                        //Send SMS   
                        }elseif($userResponse == "2"){
                        $response = "END Please wait while we place your call.\n";
                        //option 2 call
                        }elseif($userResponse == "3"){
                        $response = "END Please wait while we load your account.\n";
                        //option 3 
                        //Done
                        }
            }
        }else{
            if($level == 0) {
            //Graduate the user to the next level, so you dont serve them the same menu
             $insertQuery = "insert into `session_levels`(`session_id`, `phoneNumber`,`level`)
                values('".$sessionId."','".$phoneNumber."', 1)";
             $db->query($insertQuery);
             //Insert the phoneNumber, since it comes with the first POST
             $userInsert = "insert into `Tba`(`PhoneNumber`) values('".$phoneNumber."')";
             $db->query($userInsert);
             //Serve the menu for name
             $response = "CON Please enter your name";
          }
          elseif($level == 1) {
            //If user sends back nothing, we resend name request
             if($userResponse == "") {
                $response = "CON Name not supposed to be empty. Please enter your name \n";
             }
             else {
              //if user enters name we update the name
                $userUpdate = "update `Tba` set `Name`='".$userResponse."' where `PhoneNumber`
                  like '%". $phoneNumber ."%'";
                  $db->query($userUpdate);
                //We graduate the user to the city level
                $levelUpdate = "update `session_levels` set `level`=2
                                where `session_id`='".$sessionId."'";
                $db->query($levelUpdate);
                //We request for the city
                $response = "END Please enter your city";
             }
          }
        }
        
        //Print the response onto the page so that the ussd API/gateway can read it
        header("Content-type: text/plain");
        echo $response;
    }
    ?>