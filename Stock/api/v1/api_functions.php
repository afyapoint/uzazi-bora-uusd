<?php
/**
 *
 */
class DB_Functions
{
  private $conn;

  function __construct()
  {
    # code...
    require_once 'db_conect.php';
    $db = new DB_Connect();
    $this->conn = $db->connect();
  }
  public function addMedication(){

  }
  public function addStock($Clinic,$Medication,$Level){

    $stmt = $this->conn->prepare("INSERT INTO StockLevel(ClinicalId,MedicationId,Level) VALUES (?,?,?)");
    $stmt->bind_param("sss",$Clinic,$Medication,$Level);
    $result = $stmt->execute();
    $stmt->close();

    if ($result) {
      return $result;
    } else {
      return false;
    }
  }
  public function updateStockLevel($Clinic,$Medication,$Level){
    $stmt = $this->conn->prepare("UPDATE  StockLevel SET Level=?  WHERE MedicationId=? AND ClinicalId=?");
    $stmt->bind_param("sss",$Level,$Medication,$Clinic);
    $result = $stmt->execute();
    $stmt->close();

    if ($result) {
      return true;
    } else {
      return false;
    }

  }
  public function getAllClinics(){
    $clinics = array();
    $i = 0;
    $stmt = $this->conn->prepare("SELECT * FROM Clinics");
    $exec = $stmt->execute();
    $stmt->bind_result($Id,$Clinic,$LOcation,$Date);

    if ($exec) {
      while ($result = $stmt->fetch()) {
      $clinics[$i]["Id"]= $Id;
      $clinics[$i]["ClinicName"] = $Clinic;
      $clinics[$i]["Location"] = $LOcation;
      $clinics[$i]["Date"] = $Date;

      $i++;
      }
      $stmt->close();
      return $clinics;
    } else {
      return false;
    }

  }
  public function getAllLevels(){
    $clinics = array();
    $i = 0;
    $stmt = $this->conn->prepare("SELECT * FROM StockLevel");
    $exec = $stmt->execute();
    $stmt->bind_result($Id,$ClinicalId,$MedId,$Level,$Date);

    if ($exec) {
      while ($result = $stmt->fetch()) {
      $clinics[$i]["Id"]= $Id;
      $clinics[$i]["ClinicalId"] = $ClinicalId;
      $clinics[$i]["MedicationId"] = $MedId;
      $clinics[$i]["Level"] = $Level;
      $clinics[$i]["Date"] = $Date;

      $i++;
      }
      $stmt->close();
      for($i = 0; $i < sizeof($clinics); $i++){
        $stmt = $this->conn->prepare("SELECT * FROM Medication_Type WHERE Id = ?");
        $t =$clinics[$i]["MedicationId"];
        $stmt->bind_param("s",$t);
        $exec = $stmt->execute();
        $name = $stmt->get_result()->fetch_assoc();
        $clinics[$i]["ClinicName"] = $name["MedicationType"];
    }
      return $clinics;
    } else {
      return false;
    }

  }
  public function getStockByClinic($Clinic){
    $clinics = array();
    $i = 0;
    $stmt = $this->conn->prepare("SELECT * FROM StockLevel WHERE ClinicalId  = ?");
    $stmt->bind_param("s",$Clinic);
    $exec = $stmt->execute();
    $stmt->bind_result($Id,$ClinicId,$MedId,$Level,$Date);

    if ($exec) {
      while ($result = $stmt->fetch()) {
      $clinics[$i]["Id"]= $Id;
      $clinics[$i]["ClinicalId"] = $ClinicalId;
      $clinics[$i]["MedicationId"] = $MedId;
      $clinics[$i]["Level"] = $Level;
      $clinics[$i]["Date"] = $Date;

      $i++;
      }
      $stmt->close();
      return $clinics;
    } else {
      return false;
    }

  }
  public function getLowStock(){
    $clinics = array();
    $i = 0;
    $stmt = $this->conn->prepare("SELECT * FROM StockLevel WHERE Level  < 5 ");
    $exec = $stmt->execute();
    $stmt->bind_result($Id,$ClinicalId,$MedId,$Level,$Date);

    if ($exec) {
      while ($result = $stmt->fetch()) {
      $clinics[$i]["Id"]= $Id;
      $clinics[$i]["ClinicalId"] = $ClinicalId;
      $clinics[$i]["MedicationId"] = $MedId;
      $clinics[$i]["Level"] = $Level;
      $clinics[$i]["Date"] = $Date;

      $i++;
      }
      $stmt->close();
      for($i = 0; $i < sizeof($clinics); $i++){
        $stmt = $this->conn->prepare("SELECT * FROM Medication_Type WHERE Id = ?");
        $t =$clinics[$i]["MedicationId"];
        $stmt->bind_param("s",$t);
        $exec = $stmt->execute();
        $name = $stmt->get_result()->fetch_assoc();
        $clinics[$i]["ClinicName"] = $name["MedicationType"];
    }
      return $clinics;
    } else {
      return false;
    }
  }


  public function getStockByMedication($Medication){
    $clinics = array();
    $i = 0;
    $stmt = $this->conn->prepare("SELECT * FROM StockLevel WHERE MedicationId  = ?");
    $stmt->bind_param("s",$Medication);
    $exec = $stmt->execute();
    $stmt->bind_result($Id,$ClinicId,$MedId,$Level,$Date);

    if ($exec) {
      while ($result = $stmt->fetch()) {
      $clinics[$i]["Id"]= $Id;
      $clinics[$i]["ClinicalId"] = $ClinicalId;
      $clinics[$i]["MedicationId"] = $MedId;
      $clinics[$i]["Level"] = $Level;
      $clinics[$i]["Date"] = $Date;

      $i++;
      }
      $stmt->close();
      return $clinics;
    } else {
      return false;
    }

  }
  public function Register($Name,$Password,$Phone,$Email,$Chama,$Position){
    $stmt = $this->conn->prepare("INSERT INTO 0_Users(User_Name,Password,Phone_Number,Email,Chama,Position) VALUES (?,?,?,?,?,?)");
    $stmt->bind_param("sssss",$Name,$Password,$Phone,$Email,$Chama,$Position);
    $result = $stmt->execute();
    $stmt->close();

    if ($result) {
      return true;
    } else {
      return false;
    }
  }

  public function Login($Name,$Password)
  {
    $stmt = $this->conn->prepare("SELECT * FROM 0_Users WHERE User_Name  = ? AND Password = ?");
    $stmt->bind_param("ss",$Name,$Password);
    $result = $stmt->execute();
    $stmt->close();

    if ($result) {
      return true;
    } else {
      return false;
    }
  }
}
 ?>
