<?php
/**
* DB Connection class
*/
class  DB_Connect
{
	private $con;

	public function connect(){
		$dbport = 3306;
		 require_once 'db_config.php';
        $this->con = new mysqli(DB_Server, DB_User, DB_Password, DB_Database,$dbport);
        return $this->con;
	}
}
?>
