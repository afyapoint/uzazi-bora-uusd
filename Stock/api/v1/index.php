<?php
    require_once('api_functions.php');
    $db_funs = new DB_Functions();


    $operation = '';
    if (isset ($_GET['operation'])) {
      $operation = trim($_GET['operation']);
    } else if (isset ($_POST['operation'])) {
      $operation = trim($_POST['operation']);
    }

    switch($operation){
      case "getAllClinics":
        $response = $db_funs->getAllClinics();
        echo json_encode($response);
        break;
      case 'addStock':
        if(isset($_GET['Clinic'])){
          $Clinic = $_GET['Clinic'] ;
          $Mediction = $_GET['Medication'] ;
          $Level = $_GET['Level'] ;
          $response = $db_funs->addStock($Clinic,$Medication,$Level);
          echo json_encode($response);
        }else{
          $Clinic = $_POST['Clinic'] ;
          $Mediction = $_POST['Medication'] ;
          $Level = $_POST['Level'] ;
          $response = $db_funs->addStock($Clinic,$Medication,$Level);
          echo json_encode($response);

        }
        break;
      case 'updateStock':
        if(isset($_GET['Clinic'])){
          $Clinic = $_GET['Clinic'] ;
          $Medication = $_GET['Medication'] ;
          $Level = $_GET['Level'] ;
          $response = $db_funs->updateStockLevel($Clinic,$Medication,$Level);
          echo json_encode($response);
        }else{
          $Clinic = $_POST['Clinic'] ;
          $Mediction = $_POST['Medication'] ;
          $Level = $_POST['Level'] ;
          $response = $db_funs->updateStockLevel($Clinic,$Medication,$Level);
          echo json_encode($response);

        }
        break;
      case 'getAllStock':
          $response = $db_funs->getAllLevels();
          echo json_encode($response);
        break;
      case 'getStockByClinic':
        if(isset($_GET['Clinic'])){
            $Clinic = $_GET['Clinic'];
            $response = $db_funs->getStockByClinic($Clinic);
            echo json_encode($response);
        }else{
            $Clinic = $_POST['Clinic'];
            $response = $db_funs->getStockByClinic($Clinic);
            echo json_encode($response);
        }
        break;
      case 'getLowStock':
          $response = $db_funs->getLowStock();
          echo json_encode($response);
        break;
    }
    ?>
