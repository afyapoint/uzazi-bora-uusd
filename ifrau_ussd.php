<?php
    if(!empty($_POST)){
        require_once('db_connect.php');
        //require_once('send_message.php');
        require_once('AfricasTalkingGateway.php');
        require_once('config.php');
        
        //receiving the POST parameters
        $sessionId=$_POST['sessionId'];
        $serviceCode=$_POST['serviceCode'];
        $phoneNumber=$_POST['phoneNumber'];
        $text=$_POST['text'];
        
        $textArray = explode('*', $text);
        $userResponse = trim(end($textArray));
        
        //Check the level
        $level = 0;
        $sublevel = "0";
        $sql = "select * from `session_levels` where `session_id`='" . $sessionId . "'";
        $levelQuery = $db->query($sql);
        
        
        if($result = $levelQuery->fetch_assoc()) {
          $level = $result['level'];
          $sublevel = $result['sublevel'];
        }
        
        
        if($level == 0){
            //Graduate User to nthe next level
            $sublevel = "0";
            $level = 1;
            //$insertQuery = "insert into `session_levels`(`session_id`, `phoneNumber`,`level`,`sublevel`)
            //values('".$sessionId."','".$phoneNumber."', 1,'".$sublevel."')";
            //$db->query($insertQuery);
            $response = "CON Karibu kwenye IFRAU. Chagua moja kati ya hizi \n";
            $response .= "1. Omba usajili \n";
            $response .= "2 Kuingia ( Kama ushasajiliwa )";
            
            $stmt = $db->prepare("INSERT INTO session_levels VALUES (?,?,?,?)");
            $stmt->bind_param("ssss",$sessionId,$phoneNumber,$level,$sublevel);
            $result = $stmt->execute();
            $stmt->close();

            
        }elseif($level == 1){
            
            if($text == ""){
                $response = "CON Chagua kati ya 1 na 2. \n";
                $response .= "Andika 0 kurudi nyuma \n";
                
                //Demote user level
                $level =0;
                $sublevel = "0.1";
           
                $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
                $stmt->bind_param("sss",$level,$sublevel,$sessionId);
                $result = $stmt->execute();
                $stmt->close();

            }elseif($text == "1"){
                $response = "END Ombi lako la kusajiliwa limetuma. Utapigiwa simu kwa namba $phoneNumber kujulishewa zaidi.";
                
                //Graduate to level 2
                $level = 2;
                $sublevel = "1.1";
                $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
                $stmt->bind_param("sss",$level,$sublevel,$sessionId);
                $result = $stmt->execute();
                $stmt->close();
                
            }elseif($text == "2"){
                //Check if exsts
                $response = "CON Chagua Moja Kati ya hizi\n";
                $response .= "1. Huduma za elimu kuhusu uja uzito\n";
                $response .= "2. Nataka kumsajili mwanamke mja mzito\n";
                $response .= "3. Nataka kuitisha usaidizi kutoka kwa Daktari";
                
                //Graduate to level 2
                $level =2;
                $sublevel = "1.2";
                $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
                $stmt->bind_param("sss",$level,$sublevel,$sessionId);
                $result = $stmt->execute();
                $stmt->close();
                $_SESSION['trial'] = "trial";
                
            }

        }elseif($level == 2){
            if($userResponse == "1"){
                
                $response = "CON Chagua moja.\n";
                $response .= "1. Kujisajili katika huduma za elimu\n";
                $response .= "2 Kujiondoa katika usajili wa huduma za elimu." .  $_SESSION['trial'];;
                //To level 3
                $level =3;
                $sublevel = "1.2.1";
                
                $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
                $stmt->bind_param("sss",$level,$sublevel,$sessionId);
                $result = $stmt->execute();
                $stmt->close();

            }elseif($userResponse == "2"){
                $response = "CON Andika jina la mama mjaa mzito \n";
                
                //To level 3
                $level =3;
                $sublevel = "1.2.2";
                $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
                $stmt->bind_param("sss",$level,$sublevel,$sessionId);
                $result = $stmt->execute();
                $stmt->close();
                
                
                
            }elseif($userResponse == "3"){ 
                $response = "CON Mbona unataka kumhusisha daktari? \n";
                
                //To level 3
                $level =3;
                $sublevel = "1.2.3";
                $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
                $stmt->bind_param("sss",$level,$sublevel,$sessionId);
                $result = $stmt->execute();
                $stmt->close();
            }
        }elseif($level == 3){
            if($sublevel == "1.2.1"){
                if($userResponse == "1"){
                    $response = "END Karibu kwene mtandao w IFRAU. Utapata habari za masomo kupitia SMS bila malipo yoyote.";
                     //To level 4
                    $level =4;
                    $sublevel = "1.2.1.1";
                    
                    $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
                    $stmt->bind_param("sss",$level,$sublevel,$sessionId);
                    $result = $stmt->execute();
                    $stmt->close();
                    
                    //$code = '20880';
                    $recipients = $phoneNumber;
                    $message    = "Karibu kwenye mtandao wa IFRAU. Tutakutumia elimu kuhusu njia za kuangilia hali ya uja uzito kupitia SMS.";
                    $gateway    = new AfricasTalkingGateway($username, $api_key);
                    try { 
                        $results = $gateway->sendMessage($recipients, $message); 
                        
                    }catch ( AfricasTalkingGatewayException $e ) { 
                        echo "Encountered an error while sending: ".$e->getMessage();
                    }
                    
                }elseif($userResponse == "2"){
                    //Unsubscribe from service
                    $response = "END Umejiondoa kwenye huduma za elimu.";
                     //To level 4
                    $level =4;
                    $sublevel = "1.2.1.2";
                    
                    $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
                    $stmt->bind_param("sss",$level,$sublevel,$sessionId);
                    $result = $stmt->execute();
                    $stmt->close();
                }
                
            }elseif($sublevel == "1.2.2"){
                //Check if input is null if not store name in db
                //To level 4
                $level = 4;
                $sublevel = "1.2.2.1";
                $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
                $stmt->bind_param("sss",$level,$sublevel,$sessionId);
                $result = $stmt->execute();
                $stmt->close();
                
                if($userResponse != ""){
                    $stmt = $db->prepare("INSERT INTO Mothers(Name,session_id) VALUES(?,?)");
                    $stmt->bind_param("ss",$userResponse,$sessionId);
                    $stmt->execute();
                    $stmt->close();
                }
                
                $response = "CON Mama anaishi eneo gani? \n";
            }elseif($sublevel == "1.2.3"){
                //Check if null if not store reason in db
                //To level 4
                $level =4;
                $sublevel = "1.2.2.2";
                $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
                $stmt->bind_param("sss",$level,$sublevel,$sessionId);
                $result = $stmt->execute();
                $stmt->close();

                $response = "END Asante kwa majibu yako. Tutawasiliana nawe hivi karibuni";
            }
        }elseif($level == 4){
            if($sublevel == "1.2.2.1"){
                //To level 5
                $level = 5;
                $sublevel = "1.2.2.1.1";
                $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
                $stmt->bind_param("sss",$level,$sublevel,$sessionId);
                $result = $stmt->execute();
                $stmt->close();

                if($userResponse != ""){
                    $stmt = $db->prepare("UPDATE  Mothers SET Location=? WHERE session_id=?");
                    $stmt->bind_param("ss",$userResponse,$sessionId);
                    $stmt->execute();
                    $stmt->close();
                    
                    $stmt = $db->prepare("SELECT Id FROM Tba WHERE PhoneNumber=?");
                    $stmt->bind_param("s",$phoneNumber);
                    $result = $stmt->execute();
                    $stmt->close();
                    
                    $stmt = $db->prepare("UPDATE  Mothers SET TbaId=? WHERE session_id=?");
                    $stmt->bind_param("ss",$result,$sessionId);
                    $stmt->execute();
                    $stmt->close();
                }
                $response = "CON Kwa maoni yako. Hali ya uja uzito wa mama uko aje? \n";
                
            }
        }elseif($level == 5){
            //inserToSession($sessionId,$phoneNumber,0,"1.2.2.2.1.1");
            $level =6;
            $sublevel = "1.2.2.1.1.1";
            $stmt = $db->prepare("UPDATE  session_levels SET level=? , sublevel=? WHERE session_id=?");
             $stmt->bind_param("sss",$level,$sublevel,$sessionId);
            $result = $stmt->execute();
            $stmt->close();
            
            if($userResponse != ""){
                $author = "TBA";
                $stmt = $db->prepare("INSERT INTO  PregnancyComments(Author,AuthorId,Comment) VALUES(?,?,?)");
                $stmt->bind_param("sss",$author,$phoneNumber,$userResponse);
                $stmt->execute();
                $stmt->close();
            }            
            $response = "END Asante kwa usaidizi wako kwa kuwasaidi wamama na watoto kupata afya bora \n";
        }else{
            $response = "END Rudi nyuma";
        }
        
        //Print the response onto the page so that the ussd API/gateway can read it
        header("Content-type: text/plain");
        echo $response;
    }

?>